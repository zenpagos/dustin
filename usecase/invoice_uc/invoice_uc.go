// Package registration represents the concrete implementation of InvoiceCaseInterface interface.
// Because the same business function can be created to support both transaction and non-transaction,
// a shared business function is created in a helper file, then we can wrap that function with transaction
// or non-transaction.
package invoice_uc

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/dustin/model"
	"gitlab.com/zenpagos/dustin/repository"
	"gitlab.com/zenpagos/dustin/usecase"
)

type InvoiceCase struct {
	InvoiceRepository repository.InvoiceRepositoryInterface
}

func NewInvoiceCase(prr repository.InvoiceRepositoryInterface) usecase.InvoiceUseCaseInterface {
	return InvoiceCase{
		InvoiceRepository: prr,
	}
}

func (uc InvoiceCase) CreateInvoice(i *model.Invoice) error {
	if err := i.Validate(); err != nil {
		return err
	}

	// insert the new invoice to database
	if err := uc.InvoiceRepository.Insert(i); err != nil {
		log.WithError(err).Error()
		return err
	}

	return nil
}
