// Package usecase defines all the interfaces for a Micro-service application.
// It is the entry point for the application's business logic. It is a top level package for a Micro-service application.
// This top level package only defines interface, the concrete implementations are defined in sub-package of it.
// It only depends on model package. No other package should dependent on it except cmd.

// If transaction is supported, the transaction boundary should be defined in this package.
// A suffix-"WithTx" can be added to the name of a transaction function to distinguish it from a non-transaction one.
package usecase

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/zenpagos/dustin/model"
)

type CheckoutSessionUseCaseInterface interface {
	CreateCheckoutSession(checkoutSession *model.CheckoutSession) error
	ListCheckoutSessions(accountID uuid.UUID) ([]model.CheckoutSession, error)
	GetCheckoutSession(id uuid.UUID) (*model.CheckoutSession, error)
	GetCheckoutSessionDetail(id uuid.UUID, accountID uuid.UUID) (*model.CheckoutSession, error)
	CompleteCheckoutSession(id uuid.UUID) error
}

type InvoiceUseCaseInterface interface {
	CreateInvoice(invoice *model.Invoice) error
}
