// Package registration represents the concrete implementation of CheckoutSessionCaseInterface interface.
// Because the same business function can be created to support both transaction and non-transaction,
// a shared business function is created in a helper file, then we can wrap that function with transaction
// or non-transaction.
package checkout_session_uc

import (
	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/dustin/model"
	"gitlab.com/zenpagos/dustin/repository"
	"gitlab.com/zenpagos/dustin/usecase"
)

type CheckoutSessionCase struct {
	CheckoutSessionRepository repository.CheckoutSessionRepositoryInterface
}

func NewCheckoutSessionCase(prr repository.CheckoutSessionRepositoryInterface) usecase.CheckoutSessionUseCaseInterface {
	return CheckoutSessionCase{
		CheckoutSessionRepository: prr,
	}
}

func (uc CheckoutSessionCase) CreateCheckoutSession(cs *model.CheckoutSession) error {
	// validate items
	for _, i := range cs.Items {
		if err := i.Validate(); err != nil {
			log.WithError(err).Error()
			return err
		}
	}

	if err := cs.Validate(); err != nil {
		log.WithError(err).Error()
		return err
	}

	var total int64
	for _, i := range cs.Items {
		total += i.UnitPrice * i.Quantity
	}

	cs.Status = model.PendingStatus
	cs.Total = total

	// insert the new checkout session to database
	if err := uc.CheckoutSessionRepository.Insert(cs); err != nil {
		log.WithError(err).Error()
		return err
	}

	return nil
}

func (uc CheckoutSessionCase) ListCheckoutSessions(aid uuid.UUID) ([]model.CheckoutSession, error) {
	return uc.CheckoutSessionRepository.FindByAccountID(aid)
}

func (uc CheckoutSessionCase) GetCheckoutSession(id uuid.UUID) (*model.CheckoutSession, error) {
	return uc.CheckoutSessionRepository.FindOnePendingByID(id)
}

func (uc CheckoutSessionCase) GetCheckoutSessionDetail(id uuid.UUID, accountID uuid.UUID) (*model.CheckoutSession, error) {
	return uc.CheckoutSessionRepository.FindOneByIDAndAccountID(id, accountID)
}

func (uc CheckoutSessionCase) CompleteCheckoutSession(id uuid.UUID) error {
	return uc.CheckoutSessionRepository.UpdateToCompleteStatus(id)
}
