package config

type Configuration struct {
	Service  ServiceConfiguration
	Database DatabaseConfiguration
}
