package config

type DatabaseConfiguration struct {
	Host     string
	Username string
	Password string
	Name     string
}
