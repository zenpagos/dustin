package config

import (
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

func ReadConfig() (Configuration, error) {
	var configuration Configuration

	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath("./config")
	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("error reading config file: %s", err)
		return configuration, err
	}

	err := viper.Unmarshal(&configuration)
	if err != nil {
		log.Fatalf("unable to decode into struct: %v", err)
		return configuration, err
	}

	if configuration.Service.Prod == false {
		log.Infof("configuration %+v", configuration)
	}

	return configuration, nil
}
