package config

type ServiceConfiguration struct {
	Port int
	Prod bool
}
