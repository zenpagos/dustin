package main

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/dustin/config"
	"gitlab.com/zenpagos/dustin/container"
	"gitlab.com/zenpagos/dustin/interface/igrpc"
	"gitlab.com/zenpagos/dustin/proto/v1"
	"gitlab.com/zenpagos/dustin/repository/mysqlrepo"
	"google.golang.org/grpc"
	"google.golang.org/grpc/health/grpc_health_v1"
	"net"
)

func init() {
	log.SetReportCaller(true)
	log.SetLevel(log.InfoLevel)
	log.SetFormatter(&log.JSONFormatter{
		PrettyPrint: true,
	})
}

func main() {
	// read configuration
	conf, err := config.ReadConfig()
	if err != nil {
		log.Panicf("error getting configuration params: %v", err)
	}

	// connect to database
	db, err := mysqlrepo.NewMysqlClient(conf)
	if err != nil {
		log.Panicf("failed to connect to mysql: %v", err)
	}

	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", conf.Service.Port))
	if err != nil {
		log.Panicf("failed to listen: %v", err)
	}

	c := container.InitializeContainer(db)

	s := grpc.NewServer()

	svc := igrpc.NewGRPCServer(c)
	pbv1.RegisterDustinServiceServer(s, svc)

	healthService := igrpc.NewHealthChecker()
	grpc_health_v1.RegisterHealthServer(s, healthService)

	if err := s.Serve(lis); err != nil {
		log.Panicf("failed to serve: %v", err)
	}
}
