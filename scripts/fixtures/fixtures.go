package main

import (
	"github.com/go-testfixtures/testfixtures/v3"
	log "github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/dustin/config"
	"gitlab.com/zenpagos/dustin/model"
	"gitlab.com/zenpagos/dustin/repository/mysqlrepo"
)

func main() {
	// read configuration
	conf, err := config.ReadConfig()
	if err != nil {
		log.Panicf("error getting configuration params: %v", err)
	}

	if conf.Service.Prod == true {
		log.Panicf("this script must not be run in production mode")
	}

	// connect to database
	db, err := mysqlrepo.NewMysqlClient(conf)
	if err != nil {
		log.Panicf("failed to connect to mysql: %v", err)
	}
	defer db.Close()

	db.DropTableIfExists(
		&model.Invoice{},
		&model.CheckoutSession{},
		&model.CheckoutSessionItem{},
	)

	db.AutoMigrate(
		&model.Invoice{},
		&model.CheckoutSession{},
		&model.CheckoutSessionItem{},
	)

	fixtures, err := testfixtures.New(
		testfixtures.Database(db.DB()),
		testfixtures.Dialect(mysqlrepo.DatabaseDialect),
		testfixtures.DangerousSkipTestDatabaseCheck(),
		testfixtures.Directory("fixtures"),
	)
	if err != nil {
		log.Panicf("failed to create fixtures: %v", err)
	}

	if err := fixtures.Load(); err != nil {
		log.Panicf("failed to load fixtures: %v", err)
	}
}
