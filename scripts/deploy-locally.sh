#!/bin/bash

SVC_DEPLOYMENT=dustin

docker build -t 127.0.0.1:5000/dustin .

docker push 127.0.0.1:5000/dustin

helm upgrade --install \
  -f scripts/dustin-values.yaml \
  ${SVC_DEPLOYMENT} kubernetes/dustin
