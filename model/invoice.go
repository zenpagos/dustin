// Package model represents domain model. Every domain model type should have it's own file.
// It shouldn't depends on any other package in the application.
// It should only has domain model type and limited domain logic, in this example, validation logic. Because all other
// package depends on this package, the import of this package should be as small as possible.
package model

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	uuid "github.com/satori/go.uuid"
)

type Invoice struct {
	ID              uuid.UUID `sql:"type:varchar(255);primary_key;"`
	CreatedAt       int64     `sql:"type:int;not null"`
	Owner           uuid.UUID `sql:"type:varchar(255);not null"`
	ClientReference *string   `sql:"type:varchar(50)"`
	DueAmount       int64     `sql:"type:int;not null"`
	DueDate         *int64    `sql:"type:int"`
}

// Validate validates a newly created checkout session, which has not persisted to database yet, so Id is empty
func (i Invoice) Validate() error {
	return validation.ValidateStruct(&i,
		validation.Field(&i.CreatedAt, validation.Required),
		validation.Field(&i.Owner, validation.Required),
		validation.Field(&i.DueAmount, validation.Required),
	)
}
