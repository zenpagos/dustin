// Package model represents domain model. Every domain model type should have it's own file.
// It shouldn't depends on any other package in the application.
// It should only has domain model type and limited domain logic, in this example, validation logic. Because all other
// package depends on this package, the import of this package should be as small as possible.
package model

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
)

var currencyRule = []validation.Rule{
	validation.Required,
	validation.In(
		ARSCurrency,
		USDCurrency,
	),
}

var externalUUIDRule = []validation.Rule{
	is.UUID,
	validation.Required,
}
