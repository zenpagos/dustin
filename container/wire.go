//+build wireinject

package container

import (
	"github.com/google/wire"
	"github.com/jinzhu/gorm"
	"gitlab.com/zenpagos/dustin/repository/mysqlrepo"
	"gitlab.com/zenpagos/dustin/usecase/checkout_session_uc"
	"gitlab.com/zenpagos/dustin/usecase/invoice_uc"
)

func InitializeContainer(db *gorm.DB) Container {
	wire.Build(
		// Repositories
		mysqlrepo.NewInvoiceRepository,
		mysqlrepo.NewCheckoutSessionRepository,
		// Use Cases
		invoice_uc.NewInvoiceCase,
		checkout_session_uc.NewCheckoutSessionCase,
		NewContainer)

	return Container{}
}
