package container

import "gitlab.com/zenpagos/dustin/usecase"

type Container struct {
	InvoiceUseCase          usecase.InvoiceUseCaseInterface
	CheckoutSessionUserCase usecase.CheckoutSessionUseCaseInterface
}

func NewContainer(iuc usecase.InvoiceUseCaseInterface,
	csuc usecase.CheckoutSessionUseCaseInterface) Container {
	return Container{
		InvoiceUseCase:          iuc,
		CheckoutSessionUserCase: csuc,
	}
}
