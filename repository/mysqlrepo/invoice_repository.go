package mysqlrepo

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/zenpagos/dustin/model"
	"gitlab.com/zenpagos/dustin/repository"
)

type InvoiceRepository struct {
	DB *gorm.DB
}

func NewInvoiceRepository(db *gorm.DB) repository.InvoiceRepositoryInterface {
	return InvoiceRepository{
		DB: db,
	}
}

func (csr InvoiceRepository) Insert(cs *model.Invoice) error {
	return csr.DB.Create(&cs).Error
}
