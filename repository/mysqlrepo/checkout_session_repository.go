package mysqlrepo

import (
	"github.com/jinzhu/gorm"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/zenpagos/dustin/model"
	"gitlab.com/zenpagos/dustin/repository"
)

type CheckoutSessionRepository struct {
	DB *gorm.DB
}

func NewCheckoutSessionRepository(db *gorm.DB) repository.CheckoutSessionRepositoryInterface {
	return CheckoutSessionRepository{
		DB: db,
	}
}

func (csr CheckoutSessionRepository) Insert(cs *model.CheckoutSession) error {
	return csr.DB.Create(&cs).Error
}

func (csr CheckoutSessionRepository) UpdateToCompleteStatus(id uuid.UUID) error {
	return csr.DB.Model(&model.CheckoutSession{}).
		Where("id = ?", id.String()).
		Where("status != ?", model.ExpiredStatus).
		Update("status", model.CompletedStatus).Error
}

func (csr CheckoutSessionRepository) FindByAccountID(aid uuid.UUID) ([]model.CheckoutSession, error) {
	var css []model.CheckoutSession
	if err := csr.DB.
		Preload("Items").
		Where("account_id = ?", aid.String()).
		Find(&css).Error; err != nil {
		return nil, err
	}

	return css, nil
}

func (csr CheckoutSessionRepository) FindOnePendingByID(id uuid.UUID) (*model.CheckoutSession, error) {
	cs := new(model.CheckoutSession)
	if err := csr.DB.
		Preload("Items").
		Where("id = ?", id.String()).
		Where("status = ?", model.PendingStatus).
		First(cs).Error; err != nil {
		return nil, err
	}

	return cs, nil
}

func (csr CheckoutSessionRepository) FindOneByIDAndAccountID(id uuid.UUID, aid uuid.UUID) (*model.CheckoutSession, error) {
	cs := new(model.CheckoutSession)
	if err := csr.DB.
		Preload("Items").
		Where("id = ?", id.String()).
		Where("account_id = ?", aid.String()).
		First(cs).Error; err != nil {
		return nil, err
	}

	return cs, nil
}
