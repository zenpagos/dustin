// Package repository and it's sub-package represents data persistence service, mainly access database,
// but also including data persisted by other Micro-service.
// For Micro-server, only the interface is defined in this package, the data transformation code is in adapter package
// This is the top level package and it only defines interface, and all implementations are defined in sub-package
// Use case package depends on it.
package repository

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/zenpagos/dustin/model"
)

type CheckoutSessionRepositoryInterface interface {
	Insert(checkoutSession *model.CheckoutSession) error
	UpdateToCompleteStatus(id uuid.UUID) error
	FindByAccountID(accountID uuid.UUID) ([]model.CheckoutSession, error)
	FindOnePendingByID(id uuid.UUID) (*model.CheckoutSession, error)
	FindOneByIDAndAccountID(id uuid.UUID, accountID uuid.UUID) (*model.CheckoutSession, error)
}

type InvoiceRepositoryInterface interface {
	Insert(invoice *model.Invoice) error
}
