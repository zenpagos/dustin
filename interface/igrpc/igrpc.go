package igrpc

import (
	"context"
	_ "github.com/jnewmano/grpc-json-proxy/codec"
	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/dustin/container"
	"gitlab.com/zenpagos/dustin/model"
	"gitlab.com/zenpagos/dustin/proto/v1"
	"gitlab.com/zenpagos/tools"
)

type gRPCServer struct {
	container container.Container
}

func NewGRPCServer(c container.Container) pbv1.DustinServiceServer {
	return &gRPCServer{
		container: c,
	}
}

func (s *gRPCServer) CreateCheckoutSession(
	_ context.Context,
	req *pbv1.CreateCheckoutSessionRequest,
) (*pbv1.CreateCheckoutSessionResponse, error) {

	is := make([]model.CheckoutSessionItem, 0)
	for _, i := range req.Items {
		is = append(is, model.CheckoutSessionItem{
			Code:      tools.StringPointerOrNil(i.Code),
			Name:      i.Name,
			Quantity:  i.Quantity,
			UnitPrice: i.UnitPrice,
			ItemURL:   tools.StringPointerOrNil(i.ItemURL),
			ImageURL:  tools.StringPointerOrNil(i.ImageURL),
		})
	}

	cs := &model.CheckoutSession{
		AccountID:   uuid.FromStringOrNil(req.AccountID),
		Currency:    req.Currency,
		Reference:   req.Reference,
		Description: req.Description,
		Items:       is,
	}

	if err := s.container.CheckoutSessionUserCase.CreateCheckoutSession(cs); err != nil {
		log.WithError(err).Error()
		return nil, err
	}

	ris := make([]*pbv1.CheckoutSessionItem, 0)
	for _, i := range cs.Items {
		ris = append(ris, &pbv1.CheckoutSessionItem{
			Code:      tools.StringValueOrBlank(i.Code),
			Name:      i.Name,
			Quantity:  i.Quantity,
			UnitPrice: i.UnitPrice,
			ItemURL:   tools.StringValueOrBlank(i.ItemURL),
			ImageURL:  tools.StringValueOrBlank(i.ImageURL),
		})
	}

	res := &pbv1.CreateCheckoutSessionResponse{
		CheckoutSession: &pbv1.CheckoutSession{
			ID:          cs.ID.String(),
			CreatedAt:   cs.CreatedAt,
			UpdatedAt:   tools.Int64ValueOrZero(cs.UpdatedAt),
			ExpiresAt:   tools.Int64ValueOrZero(cs.ExpiresAt),
			Status:      cs.Status,
			Total:       cs.Total,
			AccountID:   cs.AccountID.String(),
			Currency:    cs.Currency,
			Reference:   cs.Reference,
			Description: cs.Description,
			Items:       ris,
		},
	}

	return res, nil
}

func (s *gRPCServer) ListCheckoutSessions(
	_ context.Context,
	req *pbv1.ListCheckoutSessionsRequest,
) (*pbv1.ListCheckoutSessionsResponse, error) {
	aid := uuid.FromStringOrNil(req.AccountID)

	css, err := s.container.CheckoutSessionUserCase.ListCheckoutSessions(aid)
	if err != nil {
		log.WithError(err).Error()
		return nil, err
	}

	rcs := make([]*pbv1.CheckoutSession, 0)
	for _, cs := range css {

		ris := make([]*pbv1.CheckoutSessionItem, 0)
		for _, i := range cs.Items {
			ris = append(ris, &pbv1.CheckoutSessionItem{
				Code:      tools.StringValueOrBlank(i.Code),
				Name:      i.Name,
				Quantity:  i.Quantity,
				UnitPrice: i.UnitPrice,
				ItemURL:   tools.StringValueOrBlank(i.ItemURL),
				ImageURL:  tools.StringValueOrBlank(i.ImageURL),
			})
		}

		rcs = append(rcs, &pbv1.CheckoutSession{
			ID:          cs.ID.String(),
			CreatedAt:   cs.CreatedAt,
			UpdatedAt:   tools.Int64ValueOrZero(cs.UpdatedAt),
			ExpiresAt:   tools.Int64ValueOrZero(cs.ExpiresAt),
			Status:      cs.Status,
			Total:       cs.Total,
			AccountID:   cs.AccountID.String(),
			Currency:    cs.Currency,
			Reference:   cs.Reference,
			Description: cs.Description,
			Items:       ris,
		})
	}

	res := &pbv1.ListCheckoutSessionsResponse{
		CheckoutSessions: rcs,
	}

	return res, nil
}

func (s *gRPCServer) GetPendingCheckoutSession(
	_ context.Context,
	req *pbv1.GetPendingCheckoutSessionRequest,
) (*pbv1.GetPendingCheckoutSessionResponse, error) {

	id := uuid.FromStringOrNil(req.ID)
	cs, err := s.container.CheckoutSessionUserCase.GetCheckoutSession(id)
	if err != nil {
		log.WithError(err).Error()
		return nil, err
	}

	ris := make([]*pbv1.CheckoutSessionItem, 0)
	for _, i := range cs.Items {
		ris = append(ris, &pbv1.CheckoutSessionItem{
			Code:      tools.StringValueOrBlank(i.Code),
			Name:      i.Name,
			Quantity:  i.Quantity,
			UnitPrice: i.UnitPrice,
			ItemURL:   tools.StringValueOrBlank(i.ItemURL),
			ImageURL:  tools.StringValueOrBlank(i.ImageURL),
		})
	}

	res := &pbv1.GetPendingCheckoutSessionResponse{
		CheckoutSession: &pbv1.CheckoutSession{
			ID:          cs.ID.String(),
			CreatedAt:   cs.CreatedAt,
			UpdatedAt:   tools.Int64ValueOrZero(cs.UpdatedAt),
			ExpiresAt:   tools.Int64ValueOrZero(cs.ExpiresAt),
			Status:      cs.Status,
			Total:       cs.Total,
			AccountID:   cs.AccountID.String(),
			Currency:    cs.Currency,
			Reference:   cs.Reference,
			Description: cs.Description,
			Items:       ris,
		},
	}

	return res, nil
}

func (s *gRPCServer) GetCheckoutSessionDetail(
	_ context.Context,
	req *pbv1.GetCheckoutSessionDetailRequest,
) (*pbv1.GetCheckoutSessionDetailResponse, error) {

	id := uuid.FromStringOrNil(req.ID)
	accountID := uuid.FromStringOrNil(req.AccountID)
	cs, err := s.container.CheckoutSessionUserCase.GetCheckoutSessionDetail(id, accountID)
	if err != nil {
		log.WithError(err).Error()
		return nil, err
	}

	ris := make([]*pbv1.CheckoutSessionItem, 0)
	for _, i := range cs.Items {
		ris = append(ris, &pbv1.CheckoutSessionItem{
			Code:      tools.StringValueOrBlank(i.Code),
			Name:      i.Name,
			Quantity:  i.Quantity,
			UnitPrice: i.UnitPrice,
			ItemURL:   tools.StringValueOrBlank(i.ItemURL),
			ImageURL:  tools.StringValueOrBlank(i.ImageURL),
		})
	}

	res := &pbv1.GetCheckoutSessionDetailResponse{
		CheckoutSession: &pbv1.CheckoutSession{
			ID:          cs.ID.String(),
			CreatedAt:   cs.CreatedAt,
			UpdatedAt:   tools.Int64ValueOrZero(cs.UpdatedAt),
			ExpiresAt:   tools.Int64ValueOrZero(cs.ExpiresAt),
			Status:      cs.Status,
			Total:       cs.Total,
			AccountID:   cs.AccountID.String(),
			Currency:    cs.Currency,
			Reference:   cs.Reference,
			Description: cs.Description,
			Items:       ris,
		},
	}

	return res, nil
}

func (s *gRPCServer) CompleteCheckoutSession(
	_ context.Context,
	req *pbv1.CompleteCheckoutSessionRequest,
) (*pbv1.CompleteCheckoutSessionResponse, error) {

	id := uuid.FromStringOrNil(req.ID)
	if err := s.container.CheckoutSessionUserCase.CompleteCheckoutSession(id); err != nil {
		log.WithError(err).Error()
		return nil, err
	}

	res := &pbv1.CompleteCheckoutSessionResponse{
		Completed: true,
	}

	return res, nil
}
