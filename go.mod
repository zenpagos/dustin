module gitlab.com/zenpagos/dustin

go 1.14

require (
	github.com/go-ozzo/ozzo-validation/v4 v4.1.0
	github.com/go-sql-driver/mysql v1.5.0 // indirect
	github.com/go-testfixtures/testfixtures/v3 v3.1.2
	github.com/golang/protobuf v1.3.4
	github.com/google/go-cmp v0.4.0 // indirect
	github.com/google/wire v0.4.0
	github.com/jinzhu/gorm v1.9.12
	github.com/jnewmano/grpc-json-proxy v0.0.0-20200227201450-d2f9a1e2ec3d
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/pelletier/go-toml v1.4.0 // indirect
	github.com/satori/go.uuid v1.2.0
	github.com/sirupsen/logrus v1.5.0
	github.com/spf13/viper v1.6.2
	github.com/stretchr/testify v1.5.1 // indirect
	gitlab.com/zenpagos/tools v0.0.0-20200616225453-f02e1e8b6545
	golang.org/x/crypto v0.0.0-20200302210943-78000ba7a073 // indirect
	golang.org/x/net v0.0.0-20200301022130-244492dfa37a // indirect
	golang.org/x/sys v0.0.0-20200302150141-5c8b2ff67527 // indirect
	google.golang.org/genproto v0.0.0-20200303153909-beee998c1893 // indirect
	google.golang.org/grpc v1.27.1
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
